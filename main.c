#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef struct TCar
{
    struct TCar              * m_Next;
    char                     * m_Model;
} TCAR;

typedef struct TEmployee
{
    struct TEmployee         * m_Next;
    struct TCar              * m_Car;
    char                     * m_Name;
} TEMPLOYEE;

typedef struct TOffice
{
    TEMPLOYEE                * m_Emp;
    TCAR                     * m_Car;
} TOFFICE;

#endif /* __PROGTEST__ */

TOFFICE          * initOffice   (){
    //malloc all structs
    TOFFICE *newOffice= (TOFFICE*)malloc(sizeof(TOFFICE));

    newOffice->m_Emp=NULL;//newEmployee;
    newOffice->m_Car=NULL;//newCar;

    return newOffice;
}
void               addEmployee  ( TOFFICE         * office,const char      * name ){
    TEMPLOYEE *newEmployee= (TEMPLOYEE*)malloc(sizeof(TEMPLOYEE));
    char *newEmpName=(char*)malloc(strlen(name)+3);
    strcpy(newEmpName,name);
    newEmployee->m_Name=newEmpName;
    newEmployee->m_Car=NULL;

    if(office->m_Emp==NULL){
        office->m_Emp=newEmployee;
        newEmployee->m_Next=NULL;
    }
    else{

        newEmployee->m_Next=office->m_Emp;
        office->m_Emp=newEmployee;
    }
}
void               addCar       ( TOFFICE         * office,const char      * model ){
    TCAR *newCar= (TCAR*)malloc(sizeof(TCAR));
    char *newCarName=(char*)malloc(strlen(model)+3);
    strcpy(newCarName,model);
    newCar->m_Model=newCarName;


    if(office->m_Car==NULL){
        office->m_Car=newCar;
        newCar->m_Next=NULL;
    }
    else{

        newCar->m_Next=office->m_Car;
        office->m_Car=newCar;
    }

}

TCAR * addCarToBack(TOFFICE * office,const char * model,TCAR * lastCar){
    TCAR *newCar= (TCAR*)malloc(sizeof(TCAR));
    char *newCarName=(char*)malloc(strlen(model)+3);
    strcpy(newCarName,model);
    newCar->m_Model=newCarName;////check
    newCar->m_Next=NULL;

    if(lastCar==NULL){
        office->m_Car=newCar;
    }else{
        lastCar->m_Next=newCar;
    }
    return newCar;
}
TEMPLOYEE *addEmpToBack(TOFFICE * office,const char * name,TEMPLOYEE * lastEmp){
    TEMPLOYEE *newEmployee= (TEMPLOYEE*)malloc(sizeof(TEMPLOYEE));
    char *newEmpName=(char*)malloc(strlen(name)+3);
    strcpy(newEmpName,name);
    newEmployee->m_Name=newEmpName;
    newEmployee->m_Car=NULL;//todo spojeni;
    newEmployee->m_Next=NULL;
    if(lastEmp==NULL){
        office->m_Emp=newEmployee;
    }else{
        lastEmp->m_Next=newEmployee;
    }
    return newEmployee;
}
void addEmpCarConection(TOFFICE * office,int employee,int car){
    TEMPLOYEE *tmpEmp=office->m_Emp;
    TCAR *tmpCar=office->m_Car;
    int i;
    for(i=0;i<employee;i++){
        tmpEmp=tmpEmp->m_Next;
    }
    for(i=0;i<car;i++){
        tmpCar=tmpCar->m_Next;
    }
    tmpEmp->m_Car=tmpCar;

}

void connectEmpToCar(TOFFICE *clonedOffice,TOFFICE *office){
    TEMPLOYEE *tmpEmp=office->m_Emp;
    TCAR *tmpCar=office->m_Car;
    int employee=0;
    while(tmpEmp!=NULL){
        if(tmpEmp->m_Car==NULL){
            tmpEmp=tmpEmp->m_Next;
            employee++;
            continue;
        }
        int car=0;
        while(tmpCar!=tmpEmp->m_Car){
            tmpCar=tmpCar->m_Next;
            car++;
        }
        addEmpCarConection(clonedOffice,employee,car);
        employee++;
        tmpEmp=tmpEmp->m_Next;
        tmpCar=office->m_Car;
    }
}
TOFFICE          * cloneOffice  ( TOFFICE         * office )
{
    TOFFICE *clonedOffice=NULL;
    if(office!=NULL){
        clonedOffice=initOffice();
    }else{
        return clonedOffice;
    }


    TCAR *tmpCar=NULL;
    TCAR *tmpCarPrevious=NULL;
    if(office->m_Car!=NULL){
        tmpCar=office->m_Car;
        while (tmpCar!=NULL) {

            tmpCarPrevious=addCarToBack(clonedOffice,tmpCar->m_Model,tmpCarPrevious);
            tmpCar=tmpCar->m_Next;

        }


    }


    TEMPLOYEE *tmpEmp=NULL;
    TEMPLOYEE *tmpEmpPrevious=NULL;
    if(office->m_Emp!=NULL){
        tmpEmp=office->m_Emp;
        while(tmpEmp!=NULL){
            tmpEmpPrevious=addEmpToBack(clonedOffice,tmpEmp->m_Name,tmpEmpPrevious);
            tmpEmp=tmpEmp->m_Next;
        }

    }

    connectEmpToCar(clonedOffice,office);

    return clonedOffice;
}
void               freeOffice   ( TOFFICE         * office ){

    TEMPLOYEE *tmpEmp=office->m_Emp;
    TCAR *tmpCar= office->m_Car;
    //free employees
    while(tmpEmp!=NULL){
        tmpEmp=tmpEmp->m_Next;
        free(office->m_Emp->m_Name);
        free(office->m_Emp);
        office->m_Emp=tmpEmp;

    }
    if(office->m_Emp!=NULL) {
        free(office->m_Emp->m_Name);
        free(office->m_Emp);
    }
    //free cars
    while(tmpCar!=NULL){
        tmpCar=tmpCar->m_Next;
        free(office->m_Car->m_Model);
        free(office->m_Car);
        office->m_Car=tmpCar;

    }
    if(office->m_Car!=NULL) {
        free(office->m_Car->m_Model);
        free(office->m_Car);
    }
    // free office
    free(office);

}

#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
{
    TOFFICE * a, *b,*c;
    char tmp[100];
c= initOffice();
freeOffice(c);
    assert ( sizeof ( TOFFICE ) == 2 * sizeof ( void * ) );
    assert ( sizeof ( TEMPLOYEE ) == 3 * sizeof ( void * ) );
    assert ( sizeof ( TCAR ) == 2 * sizeof ( void * ) );
    a = initOffice ();
    addEmployee ( a, "Peter" );
    addEmployee ( a, "John" );
    addEmployee ( a, "Joe" );
    addEmployee ( a, "Maria" );
    addCar ( a, "Skoda Octavia" );
    addCar ( a, "VW Golf" );
    a -> m_Emp -> m_Car = a -> m_Car;
    a -> m_Emp -> m_Next -> m_Next -> m_Car = a -> m_Car -> m_Next;
    a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Car = a -> m_Car;
    assert ( a -> m_Emp
             && ! strcmp ( a -> m_Emp -> m_Name, "Maria" )
             && a -> m_Emp -> m_Car == a -> m_Car );
    assert ( a -> m_Emp -> m_Next
             && ! strcmp ( a -> m_Emp -> m_Next -> m_Name, "Joe" )
             && a -> m_Emp -> m_Next -> m_Car == NULL );
    assert ( a -> m_Emp -> m_Next -> m_Next
             && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Name, "John" )
             && a -> m_Emp -> m_Next -> m_Next -> m_Car == a -> m_Car -> m_Next );
    assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next
             && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Name, "Peter" )
             && a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Car == a -> m_Car );
    assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    assert ( a -> m_Car
             && ! strcmp ( a -> m_Car -> m_Model, "VW Golf" ) );
    assert ( a -> m_Car -> m_Next
             && ! strcmp ( a -> m_Car -> m_Next -> m_Model, "Skoda Octavia" ) );
    assert ( a -> m_Car -> m_Next -> m_Next == NULL );
    b = cloneOffice ( a );
    strncpy ( tmp, "Moe", sizeof ( tmp ) );
    addEmployee ( a, tmp );
    strncpy ( tmp, "Victoria", sizeof ( tmp ) );
    addEmployee ( a, tmp );
    strncpy ( tmp, "Peter", sizeof ( tmp ) );
    addEmployee ( a, tmp );
    strncpy ( tmp, "Citroen C4", sizeof ( tmp ) );
    addCar ( a, tmp );
    b -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Car = b -> m_Car -> m_Next -> m_Next;
    assert ( a -> m_Emp
             && ! strcmp ( a -> m_Emp -> m_Name, "Peter" )
             && a -> m_Emp -> m_Car == NULL );
    assert ( a -> m_Emp -> m_Next
             && ! strcmp ( a -> m_Emp -> m_Next -> m_Name, "Victoria" )
             && a -> m_Emp -> m_Next -> m_Car == NULL );
    assert ( a -> m_Emp -> m_Next -> m_Next
             && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Name, "Moe" )
             && a -> m_Emp -> m_Next -> m_Next -> m_Car == NULL );
    assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next
             && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Name, "Maria" )
             && a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Car == a -> m_Car -> m_Next );
    assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next
             && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Joe" )
             && a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Car == NULL );
    assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next
             && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "John" )
             && a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Car == a -> m_Car -> m_Next -> m_Next );
    assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next
             && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Peter" )
             && a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Car == a -> m_Car -> m_Next );
    assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    assert ( a -> m_Car
             && ! strcmp ( a -> m_Car -> m_Model, "Citroen C4" ) );
    assert ( a -> m_Car -> m_Next
             && ! strcmp ( a -> m_Car -> m_Next -> m_Model, "VW Golf" ) );
    assert ( a -> m_Car -> m_Next -> m_Next
             && ! strcmp ( a -> m_Car -> m_Next -> m_Next -> m_Model, "Skoda Octavia" ) );
    assert ( a -> m_Car -> m_Next -> m_Next -> m_Next == NULL );
    assert ( b -> m_Emp
             && ! strcmp ( b -> m_Emp -> m_Name, "Maria" )
             && b -> m_Emp -> m_Car == b -> m_Car );
    assert ( b -> m_Emp -> m_Next
             && ! strcmp ( b -> m_Emp -> m_Next -> m_Name, "Joe" )
             && b -> m_Emp -> m_Next -> m_Car == NULL );
    assert ( b -> m_Emp -> m_Next -> m_Next
             && ! strcmp ( b -> m_Emp -> m_Next -> m_Next -> m_Name, "John" )
             && b -> m_Emp -> m_Next -> m_Next -> m_Car == b -> m_Car -> m_Next );
    assert ( b -> m_Emp -> m_Next -> m_Next -> m_Next
             && ! strcmp ( b -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Name, "Peter" )
             && b -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Car == NULL );
    assert ( b -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    assert ( b -> m_Car
             && ! strcmp ( b -> m_Car -> m_Model, "VW Golf" ) );
    assert ( b -> m_Car -> m_Next
             && ! strcmp ( b -> m_Car -> m_Next -> m_Model, "Skoda Octavia" ) );
    assert ( b -> m_Car -> m_Next -> m_Next == NULL );
    freeOffice ( a );
    freeOffice ( b );
    return 0;
}
#endif /* __PROGTEST__ */
